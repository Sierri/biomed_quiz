package com.example.quizbiomed;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class SummaryActivity extends Activity {

    private FirebaseFirestore nFirestore;
    private FirebaseAuth afFirestore;
    private String userID;
    private String name=MainActivityLogin.name;



    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.summary);
        int points=getIntent().getIntExtra("points",0);
        int questionNumber=getIntent().getIntExtra("questionNumber",0);
        String summary =points+"/"+questionNumber;
        TextView summaryText=findViewById(R.id.summary);
        summaryText.setText(summary);
        afFirestore = FirebaseAuth.getInstance();
        nFirestore = FirebaseFirestore.getInstance();
        userID = Objects.requireNonNull(afFirestore.getCurrentUser().getUid());
        Map<String,String> resultMap = new HashMap<>();
        LocalDate day=LocalDate.now();
        LocalTime time=LocalTime.now();

        String dateTime=day+" "+time.getHour()+":"+time.getMinute()+":"+time.getSecond();
        resultMap.put("date",dateTime);
        resultMap.put("category", QuestionActivity.categoryName);
        resultMap.put("result", String.valueOf(points));

        DocumentReference documentReference = nFirestore.collection("USERS").document(userID);
        documentReference.collection("wyniki").add(resultMap)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getApplicationContext(), "Add data to database:success",
                                Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                String error = e.getMessage();
                Toast.makeText(getApplicationContext(), "Error" + error,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onClickBack(View view) {
        Intent intent =new Intent(this, CategoryActivity.class);
        this.startActivity(intent);
    }
}