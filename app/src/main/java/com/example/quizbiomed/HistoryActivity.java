package com.example.quizbiomed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.Objects;

public class HistoryActivity extends Activity {

    private String name;
    private FirebaseFirestore nFirestore = FirebaseFirestore.getInstance();
    private FirebaseAuth afFirestore = FirebaseAuth.getInstance();
    private String userID;
    private TextView textWyniki;
    private String category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);
        textWyniki = findViewById(R.id.textViewWyniki);
        userID = Objects.requireNonNull(afFirestore.getCurrentUser().getUid());

        nFirestore.collection("USERS").document(userID).collection("wyniki")
                .orderBy("date")//wyciąganie wyników konkretnego user'a
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            String tresc = "";
                            for (QueryDocumentSnapshot document : task.getResult()){    //pętla for each po wynikach
//
                                if(document.get("category").equals("chemistry")){category = "Chemia";}
                                else if (document.get("category").equals("biology")){category = "Biologia";}
                                else if(document.get("category").equals("optics")){category = "Optyka";}
                                else if(document.get("category").equals("physics")){category = "Fizyka";}
                                tresc = tresc + System.lineSeparator()+document.get("result").toString()+"pkt"
                                        +" | " +category+" | "+ document.get("date").toString();

                            }
                            textWyniki.setText(tresc);
                        } else
                            Log.d("Users","Error getting question",task.getException());
                    }
                });



    }

    public void onClickBack(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }

}
