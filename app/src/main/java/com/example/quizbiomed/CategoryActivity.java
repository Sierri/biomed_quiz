package com.example.quizbiomed;



import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CategoryActivity extends Activity {

    private FirebaseFirestore nFirestore=FirebaseFirestore.getInstance();
    private ArrayList<Questions> questionList=new ArrayList<Questions>();
    private String name;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.name=getIntent().getStringExtra("name");
        setContentView(R.layout.activity_main);
        downloadQuestions();


    }

    public void onClickChemistry(View view) {
        Intent intent =new Intent(this, QuestionActivity.class);
        intent.putExtra("category",1);
        intent.putExtra("points",0);
        intent.putExtra("questionNumber",1);
        intent.putExtra("questionList", questionList);
        this.startActivity(intent);
    }

    public void onClickBiology(View view) {
        Intent intent =new Intent(this, QuestionActivity.class);
        intent.putExtra("category",2);
        intent.putExtra("points",0);
        intent.putExtra("questionNumber",1);
        intent.putExtra("questionList",  questionList);
        this.startActivity(intent);
    }

    public void onClickOptics(View view) {
        Intent intent =new Intent(this, QuestionActivity.class);
        intent.putExtra("category",3);
        intent.putExtra("points",0);
        intent.putExtra("questionNumber",1);
        intent.putExtra("questionList",  questionList);
        this.startActivity(intent);
    }

    public void onClickPhysics(View view) {
        Intent intent =new Intent(this, QuestionActivity.class);
        intent.putExtra("category",4);
        intent.putExtra("points",0);
        intent.putExtra("questionNumber",1);
        intent.putExtra("questionList",  questionList);
        this.startActivity(intent);
    }

    public void onClickBack(View view) {
        Intent intent =new Intent(this,MenuActivity.class);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }

    public void downloadQuestions(){                             // METODA POBIERAJACA WSZYSTKIE OBIEKTY Z BAZY I WSADZAJACA JE DO POLA ,,userList"

        nFirestore.collection("pytania")
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {        //pobieramy wszystkie dokumenty z kolekcji Users
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if(task.isSuccessful()){
                            for (QueryDocumentSnapshot document : task.getResult()){    //pętla for each po poszczególnych ściągniętych dokumentach
                                Questions question=new Questions();                                                //tworzymy nowy obiekt
                                question.setHit(Integer.parseInt(document.get("right").toString()));    //ustawiamy nowy obiekt na podstawie pobranych danych
                                question.setCategory(document.get("category").toString());
                                question.setQuestion(document.get("question").toString());
                                question.setAns1(document.get("answer1").toString());
                                question.setAns2(document.get("answer2").toString());
                                question.setAns3(document.get("answer3").toString());
                                question.setAns4(document.get("answer4").toString());
                                questionList.add(question);            //dodajemy utworzonego pojedynczego uzytkownika do listy


                            }
                        } else
                            Log.d("Users","Error getting question",task.getException());
                    }
                });

    }
}
