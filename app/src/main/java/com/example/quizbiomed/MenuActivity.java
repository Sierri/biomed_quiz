package com.example.quizbiomed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MenuActivity extends Activity {

    private String name = MainActivityLogin.name;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        TextView welcome=(TextView) findViewById(R.id.textWelcome);

        String welcomeMassage="Witaj,\n"+name+".";
        welcome.setText(welcomeMassage);
    }

    public void onClickPlay(View view) {
        Intent intent = new Intent(this, CategoryActivity.class);

        this.startActivity(intent);
    }

    public void onClickHistory(View view) {
        Intent intent = new Intent(this, HistoryActivity.class);

        this.startActivity(intent);
    }

    public void onClickAddQuestion(View view) {
        Intent intent = new Intent(this, NewQuestionActivity.class);

        this.startActivity(intent);
    }

    public void onClickLogOut(View view) {
        Intent intent = new Intent(this, MainActivityLogin.class);
        this.startActivity(intent);
    }
}
