package com.example.quizbiomed;

import java.util.ArrayList;
import java.util.List;

public class User {

    private int id;
    private String username;
    private String password;

    public User() {
    }

    public User(int id, String username, String password) {
        this.id = id;
        this.username = username;
        this.password = password;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static List<User> userList(){
        List<User> users=new ArrayList<>();
        User user1=new User(1,"Julia","julia1");
        User user2=new User(2,"Wojciech","wojciech");
        User user3=new User(3,"Marek","marek1");
        users.add(user1);
        users.add(user2);
        users.add(user3);
        return users;
    }



}

