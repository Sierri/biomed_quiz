package com.example.quizbiomed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.ArrayList;

public class MainActivityRegister extends Activity {

    private static final String TAG = "IBActivity";
    private FirebaseAuth mAuth;
    public Button signUp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_register);
        mAuth = FirebaseAuth.getInstance();
        signUp=(Button)findViewById(R.id.registerBtn);

    }


    public void onClickRegisterBtn(View view) {
    EditText edtEmail = (EditText)findViewById(R.id.emailField);
    EditText edtPassword = (EditText)findViewById(R.id.passwordField);
    EditText edtConfPassword=(EditText)findViewById(R.id.confirmPasswordField);
    String email = edtEmail.getText().toString();
    String password = edtPassword.getText().toString();
    String confPassword = edtConfPassword.getText().toString();
    TextView infoText= findViewById(R.id.infoText);

    if(email.isEmpty()||password.isEmpty()||confPassword.isEmpty()){
        infoText.setVisibility(View.VISIBLE);
        infoText.setText(R.string.empty_fields);
    }else if(!confPassword.equals(password)){
        infoText.setVisibility(View.VISIBLE);
        infoText.setText(R.string.wrong_confirm_password);
    }else {
        infoText.setVisibility(View.INVISIBLE);
        registerNewUser(email,password);
        onSuccess();
    }
    }

    public void onClickBack(View view) {
        Intent intent =new Intent(this,MainActivityLogin.class);
        this.startActivity(intent);
    }

    public void registerNewUser(String email,String password){
        mAuth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, (task) -> {
                    if (task.isSuccessful()) {
                        Log.d(TAG, "createUserWithEmail:success");
                        Toast.makeText(getApplicationContext(), "Konto zostało zarejestrowane.",
                                Toast.LENGTH_LONG).show();
                        FirebaseUser user = mAuth.getCurrentUser();
                    } else {
                        Log.w(TAG, "CreateUserWithEmail:failure", task.getException());
                        Toast.makeText(getApplicationContext(), "Błąd przy rejestracji konta.",
                                Toast.LENGTH_LONG).show();
                        Intent intent=new Intent(this,MainActivityRegister.class);
                        this.startActivity(intent);
                    }
                });
    }

    public void onSuccess(){
        EditText edtEmail = (EditText)findViewById(R.id.emailField);
        EditText edtPassword = (EditText)findViewById(R.id.passwordField);
        EditText edtConfPassword=(EditText)findViewById(R.id.confirmPasswordField);
        Button registerBtn=findViewById(R.id.registerBtn);
        registerBtn.setVisibility(View.INVISIBLE);

        ArrayList<EditText> list=new ArrayList<EditText>();
        list.add(edtEmail);
        list.add(edtPassword);
        list.add(edtConfPassword);


        for(EditText x:list){
            x.setFocusable(false);
            x.setEnabled(false);
            x.setClickable(false);
            x.setFocusableInTouchMode(false);
        }


    }

}
