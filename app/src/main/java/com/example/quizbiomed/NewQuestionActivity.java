package com.example.quizbiomed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class NewQuestionActivity extends Activity {

    private String name=MainActivityLogin.name;
    private FirebaseFirestore nFirestore;
    private FirebaseAuth afFirestore;
    private String userID;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_question);


    }

    public void onClickSend(View view) {

            Spinner categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
            Spinner hitSpinner = (Spinner) findViewById(R.id.hitNumberSpinner);
            EditText question = (EditText) findViewById(R.id.question);
            EditText ans1 = (EditText) findViewById(R.id.ans1);
            EditText ans2 = (EditText) findViewById(R.id.ans2);
            EditText ans3 = (EditText) findViewById(R.id.ans3);
            EditText ans4 = (EditText) findViewById(R.id.ans4);
            TextView info=findViewById(R.id.infoText);
            int categoryIndex = categorySpinner.getSelectedItemPosition();
            String category = selectedCategory(categoryIndex);
            int hit = hitSpinner.getSelectedItemPosition() + 1;
            if(question.getText().toString().isEmpty()||
                    ans1.getText().toString().isEmpty()||
                    ans2.getText().toString().isEmpty()||
                    ans3.getText().toString().isEmpty()||
                    ans4.getText().toString().isEmpty()
            ) {

               info.setVisibility(View.VISIBLE);
               info.setText(R.string.empty_fields);


            }
            else{
                info.setVisibility(View.INVISIBLE);
            saveQuestion(selectedCategory(categoryIndex), question.getText().toString(),
                    ans1.getText().toString(), ans2.getText().toString(),ans3.getText().toString(),
                    ans4.getText().toString(), hit);
        }
    }

    public void onClickBack(View view) {
        Intent intent = new Intent(this, MenuActivity.class);
        intent.putExtra("name",name);
        this.startActivity(intent);
    }

    public String selectedCategory(int index){

        String category;
        if (index==0){
           return category="biology";
        }
        else if(index==1){
            return category="physics";
        }
        else if(index==2){
            return category="optics";
        }
        else {
            return category="chemistry";
        }

    }

    public void saveQuestion(String category,String question,String ans1,String ans2,String ans3,String ans4,int hit){
        afFirestore = FirebaseAuth.getInstance();
        nFirestore = FirebaseFirestore.getInstance();
        userID = Objects.requireNonNull(afFirestore.getCurrentUser().getUid());
        Map<String,String> resultMap = new HashMap<>();
        resultMap.put("user ID",userID);
        resultMap.put("user name",name);
        resultMap.put("category",category);
        resultMap.put("question",question);
        resultMap.put("answer1",ans1);
        resultMap.put("answer2",ans2);
        resultMap.put("answer3",ans3);
        resultMap.put("answer4",ans4);
        resultMap.put("right",String.valueOf(hit));
        onSuccess();


        nFirestore.collection("Pytania użytkowników").add(resultMap)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Toast.makeText(getApplicationContext(), "Pytanie zostało przydzielone do oczekujących",
                                Toast.LENGTH_LONG).show();
                    }
                }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                String error = e.getMessage();
                Toast.makeText(getApplicationContext(), "Błąd" + error,
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    public void onSuccess(){
        Spinner categorySpinner = (Spinner) findViewById(R.id.categorySpinner);
        Spinner hitSpinner = (Spinner) findViewById(R.id.hitNumberSpinner);
        EditText question = (EditText) findViewById(R.id.question);
        EditText ans1 = (EditText) findViewById(R.id.ans1);
        EditText ans2 = (EditText) findViewById(R.id.ans2);
        EditText ans3 = (EditText) findViewById(R.id.ans3);
        EditText ans4 = (EditText) findViewById(R.id.ans4);
        Button send=findViewById(R.id.sendBtn);
        send.setVisibility(View.INVISIBLE);

        ArrayList<EditText> list=new ArrayList<EditText>();
        list.add(question);
        list.add(ans1);
        list.add(ans2);
        list.add(ans3);
        list.add(ans4);

        for(EditText x:list){
            x.setFocusable(false);
            x.setEnabled(false);
            x.setClickable(false);
            x.setFocusableInTouchMode(false);
        }

       categorySpinner.setFocusable(false);
        categorySpinner.setEnabled(false);
        categorySpinner.setClickable(false);
        categorySpinner.setFocusableInTouchMode(false);

        hitSpinner.setFocusable(false);
        hitSpinner.setEnabled(false);
        hitSpinner.setClickable(false);
        hitSpinner.setFocusableInTouchMode(false);

    }
}
