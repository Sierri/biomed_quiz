package com.example.quizbiomed;

public class Category {
    private String  date;
    private String category;
    private String result;

    public Category(String date, String category, String result) {
        this.date = date;
        this.category = category;
        this.result = result;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }
}
