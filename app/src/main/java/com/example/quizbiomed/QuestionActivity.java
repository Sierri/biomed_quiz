package com.example.quizbiomed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.SystemClock;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Random;


public class QuestionActivity extends Activity {

    private static final String TAG = "IBActivity";

    int hit;    //numer odpowiedzi odpowiedzialny za trafienie w danym pytaniu
    boolean score;      // czy w tym pytaniu trafiono
    int questionNumber;     //numer pytania
    int points;         //ilośc dotychczasowo zdobytych punktów
    int category;       //numer kategorii
    static String categoryName;
    int numberOfQuestions;  //liczba pytań na rundę
    public ArrayList<Questions> questions=new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
         ArrayList<Questions> questionsList = (ArrayList<Questions>) getIntent().
                 getSerializableExtra("questionList");

        initilizeMethod();

        this.points=getIntent().getIntExtra("points",0);
        this.questionNumber=getIntent().getIntExtra("questionNumber",0);
        this.numberOfQuestions=5;


      this.category=getIntent().getIntExtra("category",0);
        TextView cat=(TextView) findViewById(R.id.category);


        if (category==1){
            cat.setText("chemia");
            categoryName = "chemistry";
             questions=categoryQuestions(questionsList,categoryName);
        }else if(category==2){
            cat.setText("Biologia");
            categoryName = "biology";
             questions= categoryQuestions(questionsList,categoryName);
        }else if(category==3){
            cat.setText("Optyka");
            categoryName = "optics";
             questions= categoryQuestions(questionsList,categoryName);
        }else if(category==4){
            cat.setText("Fizyka");
            categoryName = "physics";
             questions= categoryQuestions(questionsList,categoryName);
        }
        Random random=new Random();
        int randomNumber=random.nextInt(questions.size());
        Questions question=questions.get(randomNumber);
        questionMethod(question);
        questions.remove(randomNumber);
   }




    public void onClickAns1(View view) throws InterruptedException {
        afterAnswer();
        TextView accuracyInfo=(TextView) findViewById(R.id.accuracyText);

        Button ans1=findViewById(R.id.ans1);
        Button ans2=findViewById(R.id.ans2);
        Button ans3=findViewById(R.id.ans3);
        Button ans4=findViewById(R.id.ans4);

        if (hit==1){
            accuracyInfo.setText("Dobrze!!!");
            score=true;
            accuracyInfo.setTextColor(getResources().getColor( R.color.green ));
            ans1.setBackgroundColor(getResources().getColor(R.color.green));
            ans2.setBackgroundColor(getResources().getColor(R.color.button));
            ans3.setBackgroundColor(getResources().getColor(R.color.button));
            ans4.setBackgroundColor(getResources().getColor(R.color.button));

        }
        else{
            score=false;

            accuracyInfo.setText("Zła odpowiedź :(");
            accuracyInfo.setTextColor(getResources().getColor(R.color.red));
            ans1.setBackgroundColor(getResources().getColor(R.color.red));
            ans2.setBackgroundColor(getResources().getColor(R.color.button));
            ans3.setBackgroundColor(getResources().getColor(R.color.button));
            ans4.setBackgroundColor(getResources().getColor(R.color.button));
            goodBlinking();
        }

    }


    public void onClickAns2(View view) throws InterruptedException {
        afterAnswer();
        Button ans1=findViewById(R.id.ans1);
        Button ans2=findViewById(R.id.ans2);
        Button ans3=findViewById(R.id.ans3);
        Button ans4=findViewById(R.id.ans4);

        TextView accuracyInfo=(TextView) findViewById(R.id.accuracyText);

        if (hit==2){
            score=true;
            accuracyInfo.setText("Dobrze!!!");
            accuracyInfo.setTextColor(getResources().getColor( R.color.green ));
            ans1.setBackgroundColor(getResources().getColor(R.color.button));
            ans2.setBackgroundColor(getResources().getColor(R.color.green));
            ans3.setBackgroundColor(getResources().getColor(R.color.button));
            ans4.setBackgroundColor(getResources().getColor(R.color.button));
        }
        else{
            score=false;

            accuracyInfo.setText("Zła odpowiedź :(");
            accuracyInfo.setTextColor(getResources().getColor(R.color.red));
            ans1.setBackgroundColor(getResources().getColor(R.color.button));
            ans2.setBackgroundColor(getResources().getColor(R.color.red));
            ans3.setBackgroundColor(getResources().getColor(R.color.button));
            ans4.setBackgroundColor(getResources().getColor(R.color.button));
            goodBlinking();
        }
    }


    public void onClickAns3(View view) throws InterruptedException {
        afterAnswer();
        Button ans1=findViewById(R.id.ans1);
        Button ans2=findViewById(R.id.ans2);
        Button ans3=findViewById(R.id.ans3);
        Button ans4=findViewById(R.id.ans4);
        TextView accuracyInfo=(TextView) findViewById(R.id.accuracyText);

               if (hit==3){
            score=true;
            accuracyInfo.setText("Dobrze!!!");
            accuracyInfo.setTextColor(getResources().getColor( R.color.green ));
            ans1.setBackgroundColor(getResources().getColor(R.color.button));
            ans2.setBackgroundColor(getResources().getColor(R.color.button));
            ans3.setBackgroundColor(getResources().getColor(R.color.green));
            ans4.setBackgroundColor(getResources().getColor(R.color.button));
        }
        else{
            score=false;

            accuracyInfo.setText("Zła odpowiedź :(");
            accuracyInfo.setTextColor(getResources().getColor(R.color.red));
            ans1.setBackgroundColor(getResources().getColor(R.color.button));
            ans2.setBackgroundColor(getResources().getColor(R.color.button));
            ans3.setBackgroundColor(getResources().getColor(R.color.red));
            ans4.setBackgroundColor(getResources().getColor(R.color.button));
                   goodBlinking();
        }
    }


    public void onClickAns4(View view) throws InterruptedException {
        afterAnswer();
        Button ans1=findViewById(R.id.ans1);
        Button ans2=findViewById(R.id.ans2);
        Button ans3=findViewById(R.id.ans3);
        Button ans4=findViewById(R.id.ans4);
        TextView accuracyInfo=(TextView) findViewById(R.id.accuracyText);

        if (hit==4){
            score=true;
            accuracyInfo.setText("Dobrze!!!");
            accuracyInfo.setTextColor(getResources().getColor( R.color.green ));
            ans1.setBackgroundColor(getResources().getColor(R.color.button));
            ans2.setBackgroundColor(getResources().getColor(R.color.button));
            ans3.setBackgroundColor(getResources().getColor(R.color.button));
            ans4.setBackgroundColor(getResources().getColor(R.color.green));
        }
        else{
            score=false;

            accuracyInfo.setText("Zła odpowiedź :(");
            accuracyInfo.setTextColor(getResources().getColor(R.color.red));
            ans1.setBackgroundColor(getResources().getColor(R.color.button));
            ans2.setBackgroundColor(getResources().getColor(R.color.button));
            ans3.setBackgroundColor(getResources().getColor(R.color.button));
            ans4.setBackgroundColor(getResources().getColor(R.color.red));
            goodBlinking();
        }
    }


    public void onClickBack(View view) {
        Intent intent =new Intent(this, CategoryActivity.class);
        this.startActivity(intent);
    }

    public void onClickNext(View view) {

                                                 //liczba pytań na rundę

        if (score){
            points++;
        }


        if(questionNumber<numberOfQuestions) {
            Intent intent =new Intent(this, QuestionActivity.class);
            intent.putExtra("points",points);
            questionNumber++;
            intent.putExtra("questionNumber", questionNumber);
            intent.putExtra("category",category);
            intent.putExtra("questionList", questions);
            this.startActivity(intent);
        }
        else {
            Intent intent =new Intent(this, SummaryActivity.class);
            intent.putExtra("points",points);
            intent.putExtra("category",category);
            intent.putExtra("questionNumber",questionNumber);
            this.startActivity(intent);
        }
    }



    public void initilizeMethod(){
        Button next=findViewById(R.id.nextQuestion);
        next.setVisibility(View.INVISIBLE);
        TextView accuracyInfo=(TextView) findViewById(R.id.accuracyText);
        accuracyInfo.setVisibility(View.INVISIBLE);
    }

    public void questionMethod(Questions question){
        Button ans1=findViewById(R.id.ans1);
        Button ans2=findViewById(R.id.ans2);
        Button ans3=findViewById(R.id.ans3);
        Button ans4=findViewById(R.id.ans4);
        TextView questionField=(TextView) findViewById(R.id.question);
        questionField.setText(question.getQuestion());
        ans1.setText(question.getAns1());
        ans2.setText(question.getAns2());
        ans3.setText(question.getAns3());
        ans4.setText(question.getAns4());
        hit=question.getHit();


    }

    private void afterAnswer(){
        Button ans1=findViewById(R.id.ans1);
        Button ans2=findViewById(R.id.ans2);
        Button ans3=findViewById(R.id.ans3);
        Button ans4=findViewById(R.id.ans4);

        ans1.setClickable(false);
        ans2.setClickable(false);
        ans3.setClickable(false);
        ans4.setClickable(false);
        Button next=findViewById(R.id.nextQuestion);
        if(questionNumber==numberOfQuestions){
            next.setText("Podsumowanie");
        }
        next.setVisibility(View.VISIBLE);

        TextView accuracyInfo=(TextView) findViewById(R.id.accuracyText);
        accuracyInfo.setVisibility(View.VISIBLE);

    }

    public ArrayList categoryQuestions(ArrayList<Questions> questionList,String category){
        ArrayList<Questions> categoryQuestions=new ArrayList<>();
        for(Questions question:questionList){
            if(question.getCategory().equals(category)){
                categoryQuestions.add(question);
            }
        }


        return categoryQuestions;
    }

    public void goodBlinking() throws InterruptedException {
        ArrayList<Button> answers=new ArrayList<>();
        Button ans1=findViewById(R.id.ans1);
        Button ans2=findViewById(R.id.ans2);
        Button ans3=findViewById(R.id.ans3);
        Button ans4=findViewById(R.id.ans4);

        answers.add(ans1);
        answers.add(ans2);
        answers.add(ans3);
        answers.add(ans4);
        Button btn=answers.get(hit-1);


            new CountDownTimer(2400, 400) {
                int count=1;
                public void onTick(long millisUntilFinished) {
                    if(count%2==1) {
                        btn.setBackgroundColor(getResources().getColor(R.color.green));
                    }else{
                        btn.setBackgroundColor(getResources().getColor(R.color.button));
                    }
                    count++;
                }

                public void onFinish() {
                    btn.setBackgroundColor(getResources().getColor(R.color.green));
                }
            }.start();





    }
}
