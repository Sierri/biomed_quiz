package com.example.quizbiomed;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.List;

public class MainActivityLogin extends Activity {

    private static final String TAG = "IBActivity";
    private FirebaseAuth mAuth;

    static String name;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);
        mAuth = FirebaseAuth.getInstance();
    }

    public void onClickSignInBtn(View view) {


        EditText edtUsername =(EditText) findViewById(R.id.usernameField);
        EditText edtPassword =(EditText) findViewById(R.id.passwordField);
        String email = edtUsername.getText().toString();
        String password = edtPassword.getText().toString();
        String name=email.split("@")[0];
        MainActivityLogin.name =name;


        mAuth.signInWithEmailAndPassword(email,password)
                .addOnCompleteListener(this, (task)-> {
            if(task.isSuccessful()){
                Log.d(TAG,"signInWithEmial:success");
                FirebaseUser user = mAuth.getCurrentUser();
                Toast.makeText(getApplicationContext(),"Authentication OK.",
                        Toast.LENGTH_LONG).show();
                Intent intent = new Intent(this, MenuActivity.class);
                intent.putExtra("name",name);
                this.startActivity(intent);

            }else {
                Log.d(TAG, "signInWithEmial:failure", task.getException());
                Toast.makeText(getApplicationContext(), "Authentication failed.",
                        Toast.LENGTH_LONG).show();
            }
        });

        }

    public void onClickRegisterBtn(View view) {
        Intent intent =new Intent(this,MainActivityRegister.class);
        this.startActivity(intent);
    }

    public boolean authorization(){
        List<User> users=User.userList();
        TextView infoWrong=(TextView) findViewById(R.id.infoText);
        infoWrong.setVisibility(View.INVISIBLE);
        EditText username =(EditText) findViewById(R.id.usernameField);
        EditText password =(EditText) findViewById(R.id.passwordField);
        boolean authorization=false;
        String usernameString=username.getText().toString();
        String passwordString=password.getText().toString();

        for(int i=0;i<users.size();i++){
            if(users.get(i).getUsername().toUpperCase().equals(usernameString.toUpperCase())){
                if (users.get(i).getPassword().equals(passwordString)){
                    authorization=true;
                }
            }
        }
        return authorization;
    }

}
